package app;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@Path("/api/establishments/v1/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "establishments", description = "Operations on establishments resource.")
public class EstablishmentResource {

    private static final String UUID_PATTERN =
            "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    @GET
    @Operation(summary = "Get all establishments")
    public List<Establishment> get() {
        return Establishment.listAll();
    }

    @GET
    @Path("{uuid}")
    @APIResponse(responseCode = "200")
    @APIResponse(responseCode = "404", description = "Establishment not found")
    @Operation(summary = "Find establishment by ID")
    public Establishment getSindle(@Parameter(schema = @Schema(format = "uuid", type = SchemaType.STRING,
            pattern = UUID_PATTERN)) @PathParam("uuid") UUID uuid) {
        Establishment entity = Establishment.findByUUID(uuid);

        if (entity == null) {
            throw new WebApplicationException("Establishment not found", Status.NOT_FOUND);
        }

        return entity;
    }

    @POST
    @Transactional
    @APIResponse(responseCode = "201", description = "Establishment created")
    @APIResponse(responseCode = "406", description = "Invalid data")
    @Operation(summary = "Create new establishment")
    public Response create(@Valid Establishment entity) {
        if (entity.uuid != null) {
            throw new WebApplicationException("Id was invalidly set on request",
                    Status.NOT_ACCEPTABLE);
        }

        entity.persist();

        return Response.ok(entity).status(Status.CREATED).build();
    }

    @PUT
    @Path("{uuid}")
    @Transactional
    @APIResponse(responseCode = "200")
    @APIResponse(responseCode = "404", description = "Establishment not found")
    @Operation(summary = "Edit establishment by ID")
    public Establishment update(@Parameter(schema = @Schema(format = "uuid", type = SchemaType.STRING,
            pattern = UUID_PATTERN)) @PathParam("uuid") UUID uuid, @Valid Establishment newEntity) {
        Establishment entity = Establishment.findByUUID(uuid);

        if (entity == null) {
            throw new WebApplicationException("Establishment not found", Status.NOT_FOUND);
        }

        return entity;
    }

    @DELETE
    @Path("{uuid}")
    @APIResponse(responseCode = "204", description = "Establishment deleted")
    @APIResponse(responseCode = "404", description = "Establishment not found")
    @Operation(summary = "Delete establishment by ID")
    public Response delete(@Parameter(schema = @Schema(format = "uuid", type = SchemaType.STRING,
            pattern = UUID_PATTERN)) @PathParam("uuid") UUID uuid) {
        Establishment entity = Establishment.findByUUID(uuid);

        if (entity == null) {
            throw new WebApplicationException("Establishment not found", Status.NOT_FOUND);
        }

        entity.delete();

        return Response.status(Status.NO_CONTENT).build();
    }
}
